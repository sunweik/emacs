(when window-system
  (set-frame-size (selected-frame) 140 60)
  (set-face-attribute 'default nil :font "DejaVu Sans Mono-12")
  (set-fontset-font t 'unicode (font-spec :name "heiti sc-14")))

(let ((default-directory  "~/.emacs.d/site-lisp/"))
      (normal-top-level-add-subdirs-to-load-path))

;; set keys for Apple keyboard, for emacs in OS X
(if (eq system-type 'darwin)
  (setq mac-command-modifier 'super)   ; make cmd key do Super
  (setq mac-option-modifier 'meta)     ; make opt key do Meta
  (setq mac-control-modifier 'control) ; make Control key do Control
  (setq ns-function-modifier 'hyper))  ; make Fn key do Hyper

(load-file "~/.emacs.d/site-lisp/key.el")

(load-file "~/.emacs.d/site-lisp/plugin.el")

(load-file "~/.emacs.d/site-lisp/misc.el")

;;Set grep-find command
;;(setq grep-find-command "find . ( -name \\*.cpp -o -name \\*.h ) -type f -print0 | xargs -0 -e grep -niH -e ")
(setq grep-find-command "find . -name \"*.cpp\" -o -name \"*.h\" -o -name \"*.c\"  | xargs grep -ni -e ")

(setq js-indent-level 4)

(global-hl-line-mode 1)
;;(set-face-attribute hl-line-face nil :underline t)
(set-face-background 'highlight "#222")
(set-face-background 'highlight nil)
(set-face-foreground 'highlight nil)
(set-face-underline-p 'highlight t)

