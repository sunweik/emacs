(defvar farm-dif-result nil
   "Store all the result from farm showdiffs")

(defvar farm-dif-buf-name "*FARM DIF*"
  "The name of buffer in which farm dif displays.")

(defvar farm-jobs-buf-name "*FARM JOBS*"
  "The name of buffer in which farm jobs displays.")

(defvar farm-dif-root-folder ""
  "The root folder of all dif files")

(defvar farm-jobs-first-job-line 0)
(defvar farm-jobs-last-job-line 0)


;; ;; ;;
;; ;; ;; End of user option varaibles
;; ;; ;;

;; (defun farm-dif-show())

;; (defun farm-dif-quit())

;; ;; Go through whole buffer and get root dir of dif
;; (defun farm-dif-get-root-dir()
;;                                         ;  (defvar rootdir "")
;;                                         ;  (defvar subdir "")
;;                                         ;  (defvar difname "")
;;                                         ;  (defvar fulldifname "")

;;   (defun test-buffer ()
;;     (set-buffer "test")
;;     (goto-char (point-min))

;;     ;;Go through buffer line by line
;;     (setq flag nil)
;;     (while (or flag
;;                (not (= (point) (point-max))))
;;       (setq line (buffer-substring (line-beginning-position) (line-end-position)))
;;       (when (string-match "^Results in[ \\t]+\\(.*\\)" line)
;;         (setq farm-dif-root-folder (match-string 1 line))
;;         (setq flag true))

;;       ;;       (when (string-match "^\\([A-Za-z0-9]+\\)[ \\t]+\\([A-Za-z0-9]+.dif\\)" line)
;;       ;;         (setq subdir (match-string 1 line))
;;       ;;         (setq difname (match-string 2 line))
;;       ;;         (setq fulldifname (concat rootdir "/" subdir "/" difname)))

;;       (forward-line))))

;; (defun msg-me (process event)
;;   (farm-dif-mode))

;; (defun keep-output (process output)
;;   (setq farm-dif-result (cons output teststr)))

;; (defun farm-dif-fetch-by-job (jobname &optional noselect)
;;   (interactive "sJob Name: ")

;;   (let ((command        "showdiffs")
;;         (arg1           "-job"))
;;     (get-buffer-create farm-dif-buf-name)
;; ;;    (get-buffer-create buf)
;; ;;    (funcall (if noselect #'display-buffer #'switch-to-buffer) buf)
;;     (set-buffer farm-dif-buf-name)
;; ;;    (toggle-read-only nil)
;;     (erase-buffer)
;; ;;    (toggle-read-only t)

;;     ;;the farm command
;;     (start-process "job" farm-dif-buf-name "sleep" "")
;;     (set-process-filter (get-process "job") 'keep-output)
;;     (set-process-sentinel (get-process "job") 'msg-me)
;;     (start-process "job" farm-dif-buf-name "farm" command arg1 jobname)

;;     ;;hook the buffer processor
;;     ))

(defun farm-find-first-job-line ()
  ;;Find first effective line of report
  (goto-char (point-min))
  (setq count 0)
  (while (and (< count 2)
              (< (point) (point-max)))
    (setq line (buffer-substring (line-beginning-position) (line-end-position)))
    (if (eq (string-match "---\\( \\|-\\).+" line) 0)
        (progn
          (setq count (+ count 1))))
    (forward-line))
  (setq farm-jobs-first-job-line (point)))

(defun farm-find-last-job-line ()
  ;;Find first effective line of report
  (goto-char (point-max))
  (setq flag 'true)
  (while (and flag
              (> (point) (point-min)))
     (setq line (buffer-substring (line-beginning-position) (line-end-position)))
     (if (eq (string-match "[1-9][0-9]\\{6\\}.*[A-Z]\\{3\\}.*farms.*[1-9]" line) 0)
         (setq flag nil))
      (previous-line))
  (forward-line)
  (setq farm-jobs-last-job-line (point)))

(defun farm-next-line ()
  (interactive)

  (put-text-property (line-beginning-position) (line-end-position) 'face nil (current-buffer))

  (next-line)
  (setq line (buffer-substring (line-beginning-position) (line-end-position)))

  (if (not (eq (string-match "[1-9][0-9]\\{6\\}.*[A-Z]\\{3\\}.*farms.*[1-9]" line) 0))
    (progn
      (goto-char farm-jobs-first-job-line)))
  (put-text-property (line-beginning-position) (line-end-position) 'face 'hi-yellow (current-buffer)))

(defun farm-previous-line ()
  (interactive)

  (put-text-property (line-beginning-position) (line-end-position) 'face nil (current-buffer))

  (previous-line)
  (setq line (buffer-substring (line-beginning-position) (line-end-position)))

  (if (not (eq (string-match "[1-9][0-9]\\{6\\}.*[A-Z]\\{3\\}.*farms.*[1-9]" line) 0))
    (progn
      ;;Find first effective line of report
      (goto-char farm-jobs-last-job-line)))
  (put-text-property (line-beginning-position) (line-end-position) 'face 'hi-yellow (current-buffer)))

(defun farm-show-difs (job)
  (princ job)
  )

(defun farm-grab-difs ()
  (interactive)

  (setq line (buffer-substring (line-beginning-position) (line-end-position)))
  (string-match "\\(^[1-9][0-9]\\{6\\}\\).*farms.*" line)
  (farm-show-difs (match-string-no-properties 1 line)))

(defvar farm-mode-map
  (let ((map (make-sparse-keymap)))
    (suppress-keymap map)
    (define-key map "n" 'farm-next-line)
    (define-key map "p" 'farm-previous-line)
    (define-key map (kbd "RET") 'farm-grab-difs)
    map)
  "Mode map used for the buffer created by `color-theme-select'.")

(defun farm-jobs-output-filter (proc string)
  (with-current-buffer (process-buffer proc)
    (insert string)))

(defun farm-jobs-sentinel (process event)
  (set-buffer farm-jobs-buf-name)
  (insert "\n")
  (insert (format " Process: %s had the event `%s'" process event))
  (farm-dif-mode))

(defun farm-dif-mode ()
  (switch-to-buffer-other-window farm-jobs-buf-name)
  (goto-char (point-min))
  (delete-char 0)
  (message "Commands: m, u, t, RET, g, k, S, D, Q; q to quit; h for help")

  (kill-all-local-variables)
  (setq major-mode 'farm-mode)
  (setq mode-name "Farm")
  (use-local-map farm-mode-map)
  (farm-find-first-job-line)
  (farm-find-last-job-line)

  (goto-char farm-jobs-first-job-line)
  )

;; farm showjobs
(defun farm-show-jobs ()
  (get-buffer-create farm-jobs-buf-name)
  (with-current-buffer farm-jobs-buf-name (erase-buffer))
  ;;  (set-buffer farm-jobs-buf-name)

  ;;the farm command
  (let ((proc (start-process "showjobs" farm-jobs-buf-name "farm" "showjobs")))
    (set-process-filter proc 'farm-jobs-output-filter)
    (set-process-sentinel proc 'farm-jobs-sentinel)
    (while (eq (process-status proc) 'run)
      (accept-process-output proc)))
  (farm-find-last-job-line)
  (farm-find-first-job-line))

(farm-show-jobs)

;;(farm-dif-fetch-by-job "2345061")

;; (if (eq (string-match "[1-9][0-9]\\{6\\}.*[A-Z]\\{3\\}.*farms.*[1-9]"
;;                       "2854045 MAY 15 16:56 farms              wsun_bug-8502010 running    1     12/13") 0)
;;     (setq flag nil))

;; (string-match "--\\(-\\| \\)+"
;;               "---- ---------")

;; (princ teststr)

;; (let ((s "Alex"))
;;       (put-text-property 0 (length s) 'face 'bold s)
;;       (insert s))

;; (put-text-property 1000 5000 'face 'highlight (current-buffer))
;; (put-text-property 1000 5000 'face nil (current-buffer))
