;;Disable startup message
(setq inhibit-startup-message t)

;;Load gnuclietw for windows
(server-start)

;;Set the scroll 1 line for each time
(setq scroll-conservatively 10000
	  scroll-margin 0)

;;Unload the toolbar and scrollbar
(tool-bar-mode -1)
(scroll-bar-mode -1)
(if (not (eq system-type 'darwin))
		(menu-bar-mode -1))

;;Set the full filename in the title
(setq frame-title-format "emacs@%b,%f")

;; "y or n" instead of "yes or no"
(fset 'yes-or-no-p 'y-or-n-p)

;;Set backup file directory
(setq
   backup-by-copying t      ; don't clobber symlinks
   backup-directory-alist
    '(("." . "~/.emacs.d/backup"))    ; don't litter my fs tree
;; auto-save-file-name-transforms
;;   '((".*", temporary-file-directory t))
   delete-old-versions t
   kept-new-versions 6
   kept-old-versions 2
   version-control t)

;; Save all tempfiles in $TMPDIR/emacs$UID/
(defconst emacs-tmp-dir (format "%s%s%s/" temporary-file-directory "emacs" (user-uid)))
(setq backup-directory-alist
      `((".*" . ,emacs-tmp-dir)))
(setq auto-save-file-name-transforms
      `((".*" ,emacs-tmp-dir t)))
(setq auto-save-list-file-prefix
      emacs-tmp-dir)

;;Enable Display picture
(auto-image-file-mode)

;;Enable mouse avoid cursor automatically
(mouse-avoidance-mode 'animate)

;;Set a big kill-ring
(setq kill-ring-max 500)

;;display the line number
(setq column-number-mode t)

;;higtlight the selected
(transient-mark-mode t)

;;close the bell
(setq-default visible-bell t)

;;Display time
(display-time)

;; Font-Lock
;; (if window-system
;;	   (progn
;;		 (setq font-lock-support-mode 'lazy-lock-mode)
;;		 (global-font-lock-mode t)))
;;(setq font-lock-support-mode 'lazy-lock-mode)
(global-font-lock-mode t)

;;Show a kill-ring buffer
;;Load hippie-expand
(global-set-key [(meta ?/)] 'hippie-expand)
(setq hippie-expand-try-functions-list
	  '(try-expand-dabbrev
		try-expand-dabbrev-visible
		try-expand-dabbrev-all-buffers
		try-expand-dabbrev-from-kill
		try-complete-file-name-partially
		try-complete-file-name
		try-expand-all-abbrevs
		try-expand-list
		try-expand-line
		try-complete-lisp-symbol-partially
		try-complete-lisp-symbol))

;;I like add a soft "return" for long lines
(setq-default truncate-partial-width-windows nil)
(auto-fill-mode nil)

;;dired-mode's setting
(put 'dired-find-alternate-file 'disabled nil)

;;Set second window's height
(setq-default compilation-window-height 10)
(setq-default grep-window-height 10)
(setq-default compilation-scroll-output t)

;;Set paren mode
(show-paren-mode 1)
(setq-default show-paren-delay 0)

;;about the txt edit's enviroment
(setq-default indent-tabs-mode nil)
(setq-default default-tab-width 2)

(setq-default visible-bell nil)

;;Set Desktop reloading
(load "desktop")
(desktop-save-mode 1)
(setq-default history-length 500)

(setq-default show-trailing-whitespace t)

;; ;; color theme
;; (when window-system
;;   (load-theme 'misterioso))

;;Load ibuffer
(global-set-key (kbd "C-x C-b") 'ibuffer)
;;Load ido("interactively do")
(ido-mode t)

(global-linum-mode)
(global-hl-line-mode 1)
(set-face-background 'highlight nil)
(set-face-foreground 'highlight nil)
(set-face-underline-p 'highlight t)


;; turn off init-scratch-message
(setq initial-scratch-message "")
;; text mode for scratch
(setq initial-major-mode 'text-mode)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

;; stop grep/compile start new window
(setq split-height-threshold nil
      split-width-threshold nil)

;; enable hideshow mode
(add-hook 'prog-mode-hook #'hs-minor-mode)
(setq hs-isearch-open 'code)

;; ;; enable semantic mode
;; (semantic-mode)
;; (global-semantic-stickyfunc-mode)
;; (global-semantic-highlight-func-mode)
;; (global-semantic-decoration-mode)
