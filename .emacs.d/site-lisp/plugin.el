;;Simulate 'f' command in vi
(defun wy-go-to-char (n char)
  "Move forward to Nth occurence of CHAR.
Typing `wy-go-to-char-key' again will move forwad to the next Nth
occurence of CHAR."
  (interactive "p\ncGo to char: ")
  (search-forward (string char) nil nil n)
  (while (char-equal (read-char)
					 char)
	(search-forward (string char) nil nil n))
  (setq unread-command-events (list last-input-event)))

(define-key global-map (kbd "C-c a") 'wy-go-to-char)

;;(defvar tr-image-directory "~/site-lisp/tree/images/")
;;(require 'tree-dir)

(when (> emacs-major-version 23)
	(require 'package)
	(package-initialize)
	(add-to-list 'package-archives
							 '("melpa" . "http://melpa.milkbox.net/packages/") t)
	(add-to-list 'package-archives
							 '("marmalade" .
								 "http://marmalade-repo.org/packages/") t)

  ;; (unless (package-installed-p 'better-defaults)
	;; 	(package-refresh-contents) (package-install 'better-defaults))
  (unless (package-installed-p 'scala-mode2)
    (package-refresh-contents) (package-install 'scala-mode2))
  (unless (package-installed-p 'bm)
		(package-refresh-contents) (package-install 'bm))
  (unless (package-installed-p 'bookmark+)
		(package-refresh-contents) (package-install 'bookmark+))
  (unless (package-installed-p 'browse-kill-ring)
		(package-refresh-contents) (package-install 'browse-kill-ring))
  (unless (package-installed-p 'session)
		(package-refresh-contents) (package-install 'session))
  (unless (package-installed-p 'highlight-symbol)
		(package-refresh-contents) (package-install 'highlight-symbol))
  (unless (package-installed-p 'auto-complete)
		(package-refresh-contents) (package-install 'auto-complete))
  (unless (package-installed-p 'window-number)
		(package-refresh-contents) (package-install 'window-number))
  (unless (package-installed-p 'google-c-style)
		(package-refresh-contents) (package-install 'google-c-style))
  (unless (package-installed-p 'ag)
		(package-refresh-contents) (package-install 'ag))
  (unless (package-installed-p 'js2-mode)
		(package-refresh-contents) (package-install 'js2-mode))
  (unless (package-installed-p 'ac-js2)
		(package-refresh-contents) (package-install 'ac-js2))
  (unless (package-installed-p 'smartparens)
    (package-refresh-contents) (package-install 'smartparens))
  ;; (unless (package-installed-p 'paredit)
	;; 	(package-refresh-contents) (package-install 'paredit))
  (unless (package-installed-p 'exec-path-from-shell)
		(package-refresh-contents) (package-install 'exec-path-from-shell))
  (unless (package-installed-p 'flx-ido)
		(package-refresh-contents) (package-install 'flx-ido))
  (unless (package-installed-p 'projectile)
		(package-refresh-contents) (package-install 'projectile))
  )

;; ctrlp in emacs
(projectile-global-mode)

;; load exec-path from shell
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

;; highlight current symbol
;;(auto-highlight-symbol-mode t)
(require 'highlight-symbol)
(highlight-symbol-mode) ;; automatic highlight
(global-set-key [(control f4)] 'highlight-symbol-at-point)
(global-set-key [f4] 'highlight-symbol-next)
(global-set-key [(shift f4)] 'highlight-symbol-prev)
(global-set-key [(meta f4)] 'highlight-symbol-query-replace)

(require 'browse-kill-ring)
(global-set-key [(control c)(k)] 'browse-kill-ring)
(browse-kill-ring-default-keybindings)

;;Load session script
(require 'session)
(add-hook 'after-init-hook 'session-initialize)

;; auto complete
(require 'auto-complete-config)
(add-to-list 'ac-dictionary-directories "~/.emacs.d/dict")
(set-default 'ac-sources
             '(ac-source-abbrev
               ac-source-dictionary
               ac-source-yasnippet
               ac-source-words-in-buffer
               ac-source-words-in-same-mode-buffers
               ac-source-semantic))
(ac-config-default)
(dolist (m '(c-mode c++-mode java-mode))
  (add-to-list 'ac-modes m))
(global-auto-complete-mode t)

;; (require 'bookmark+)

;;======================================================================
;;Setup highlight bookmark
;;In order to persisent bookmark, bm.el should be loaded before desktop
;; make bookmarks persistent as default
(setq-default bm-buffer-persistence t)
;; Make sure the repository is loaded as early as possible
(setq-default bm-restore-repository-on-load t)
(require 'bm)

;;; Configuration:
;;
;;   To make it easier to use, assign the commands to some keys.
;;
;;   M$ Visual Studio key setup.
(global-set-key (kbd "<C-f2>") 'bm-toggle)
(global-set-key (kbd "ESC <f2>") 'bm-toggle)
(global-set-key (kbd "<f2>")   'bm-next)
(global-set-key (kbd "<S-f2>") 'bm-previous)

;; Restoring bookmarks when on file find.
(add-hook 'find-file-hooks 'bm-buffer-restore)

;; Saving bookmark data on killing a buffer
(add-hook 'kill-buffer-hook 'bm-buffer-save)

;; Saving the repository to file when on exit.
;; kill-buffer-hook is not called when emacs is killed, so we
;; must save all bookmarks first.
(add-hook 'kill-emacs-hook '(lambda nil
 		             (bm-buffer-save-all)
		             (bm-repository-save)))

;; Update bookmark repository when saving the file.
(add-hook 'after-save-hook 'bm-buffer-save)

;; Restore bookmarks when buffer is reverted.
(add-hook 'after-revert-hook 'bm-buffer-restore)

;; make sure bookmarks is saved before check-in (and revert-buffer)
(add-hook 'vc-before-checkin-hook 'bm-buffer-save)

;; google cpp style
(add-hook 'c-mode-common-hook 'google-set-c-style)
(add-hook 'c-mode-common-hook 'google-make-newline-indent)

;; setup window number
(require 'window-number)
(window-number-mode)
(window-number-meta-mode)

;; enable js2-mode
(add-hook 'js-mode-hook 'js2-minor-mode)
(add-hook 'js2-mode-hook 'ac-js2-mode)
(setq js2-highlight-level 3)
(eval-after-load 'js
  '(define-key js-mode-map (kbd "C-M-h") 'js2-mark-defun))

;; enable smartparens
;; electric-pair-mode a okay candidate
(load-file "~/.emacs.d/site-lisp/sp.el")


